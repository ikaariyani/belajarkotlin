fun main(args: Array<String>) {
    //input
    print("Enter your DOB:")
    var DOB:Int= readLine()!!.toInt()

    //process
    var year = 2007
    var age:Int?
    age = year-DOB

    //output
    println("Your age is $age years")
}
