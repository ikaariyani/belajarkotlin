/**
 * Created by labsaintek on 10/17/2017.
 */
fun main(args: Array<String>) {
    var arraylist= ArrayList<String>()
    arraylist.add("Ika")
    arraylist.add("Ariyani")
    arraylist.add("Jepara")

    println("First name:" + arraylist.get(0))

    println("all element by object")
    for (item in arraylist) {
        println(item)
    }

    arraylist.set(0, "Unisnu Jepara")

    println("all element by index")
    for (index in 0..arraylist.size-1) {
        println(arraylist.get(index))
    }

    //search
    if(arraylist.contains ("Ruby")) {
        println("name is found")
    }else{
        println("name is not found")
    }
}